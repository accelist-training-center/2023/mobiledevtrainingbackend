﻿using MobileDevTraining.Web.Models;

namespace MobileDevTraining.Web.Data
{
    public class StudentProvider
    {
        public List<StudentDataModel> Students { get; set; } = new List<StudentDataModel>();
    }
}
