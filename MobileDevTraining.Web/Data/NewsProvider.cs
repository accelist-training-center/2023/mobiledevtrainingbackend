﻿using MobileDevTraining.Web.Models;

namespace MobileDevTraining.Web.Data
{
    public class NewsProvider
    {
        private static readonly TimeSpan JakartaTimezoneSpan = new TimeSpan(7, 0, 0);

        public List<HomeNewsModel> NewsList { get; set; } = new List<HomeNewsModel>
            {
                new HomeNewsModel
                {
                    Id = 1,
                    Title = "Kurikulum mobile app development dengan React Native dan Expo telah diselenggarakan",
                    Timestamp = new DateTimeOffset(2023, 5, 15, 0, 0, 0, JakartaTimezoneSpan)
                },
                new HomeNewsModel
                {
                    Id = 2,
                    Title = "Survey training 3 + 1 2023",
                    Timestamp = new DateTimeOffset(2023, 5, 5, 0, 0, 0, JakartaTimezoneSpan)
                }
            };
    }
}
