﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MobileDevTraining.Web.Data;
using MobileDevTraining.Web.Models;

namespace MobileDevTraining.Web.Controllers
{
    [Route("api/v1/student")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly StudentProvider _studentProvider;

        public StudentController(StudentProvider studentProvider)
        {
            _studentProvider = studentProvider;
        }

        [HttpGet]
        public ActionResult<List<StudentDataModel>> Get()
        {
            return Ok(_studentProvider.Students);
        }

        [HttpPost]
        public ActionResult Post([FromBody]AddStudentFormModel newStudent)
        {
            // Don't forget to validate input from client!
            _studentProvider.Students.Add(new StudentDataModel
            {
                Id = Guid.NewGuid(),
                Name = newStudent.Name!,
                Weight = newStudent.Weight,
                Address = newStudent.Address!
            });

            return Ok();
        }
    }
}
