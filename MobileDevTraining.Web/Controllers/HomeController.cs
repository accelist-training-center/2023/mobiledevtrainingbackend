﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MobileDevTraining.Web.Data;
using MobileDevTraining.Web.Models;

namespace MobileDevTraining.Web.Controllers
{
    [Route("api/v1/home")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly NewsProvider _newsProvider;

        public HomeController(NewsProvider newsProvider)
        {
            _newsProvider = newsProvider;
        }

        [HttpGet]
        public ActionResult<List<HomeNewsModel>> Get()
        {
            var news = _newsProvider.NewsList;

            return Ok(news);
        }
    }
}
