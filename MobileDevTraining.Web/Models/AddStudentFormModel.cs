﻿namespace MobileDevTraining.Web.Models
{
    public class AddStudentFormModel
    {
        public string? Name { get; set; }

        public int Weight { get; set; }

        public string? Address { get; set; }
    }
}
