﻿namespace MobileDevTraining.Web.Models
{
    public class StudentDataModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; } = string.Empty;
        
        public int Weight { get; set; }

        public string Address { get; set; } = string.Empty;
    }
}
